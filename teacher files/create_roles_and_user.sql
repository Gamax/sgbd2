create role sgbd2_role not identified;
grant alter session to sgbd2_role;
grant create database link to sgbd2_role;
grant create session to sgbd2_role;
grant create procedure to sgbd2_role;
grant create sequence to sgbd2_role;
grant create table to sgbd2_role;
grant create trigger to sgbd2_role;
grant create type to sgbd2_role;
grant create synonym to sgbd2_role;
grant create view to sgbd2_role;
grant create job to sgbd2_role;
grant create materialized view to sgbd2_role;
-- https://docs.oracle.com/en/database/oracle/oracle-database/12.2/sqlrf/CREATE-DIRECTORY.html
grant create any directory to sgbd2_role;
grant execute on sys.dbms_lock to sgbd2_role;
grant execute on sys.owa_opt_lock to sgbd2_role;

-- CC
create user hopital_lmd identified by oracle account unlock;
alter user hopital_lmd quota unlimited on users;
grant sgbd2_role to hopital_lmd;