DROP TABLE Specialites CASCADE CONSTRAINTS;
DROP TABLE Medecins CASCADE CONSTRAINTS;
DROP TABLE Patients CASCADE CONSTRAINTS;
DROP TABLE Services CASCADE CONSTRAINTS;
DROP TABLE Chambres CASCADE CONSTRAINTS;
DROP TABLE PatientsChambres CASCADE CONSTRAINTS;

CREATE TABLE Specialites
(
  Specialite NUMBER(4)
    CONSTRAINT PKSpecialites PRIMARY KEY,
  Libelle    VARCHAR2(30)
);

CREATE TABLE Medecins
(
  NumOrdre    CHAR(10)
    CONSTRAINT PKMedecinsNumOrdre PRIMARY KEY,
  Nom         VARCHAR2(20)
    CONSTRAINT NNMedecinsNom NOT NULL,
  Prenom      VARCHAR2(20)
    CONSTRAINT NNMedecinsPrenom NOT NULL,
  DateDiplome DATE
    CONSTRAINT NNMedecinsDateDiplome NOT NULL,
  Specialite  NUMBER(4)
    CONSTRAINT FKMedecinsSpecialites REFERENCES Specialites (Specialite),
  NumService  CHAR(4)
    CONSTRAINT NNMedecinsNumService NOT NULL,
  CONSTRAINT CKMedecinsNumOrdre CHECK ( substr(NumOrdre,1,6) = to_char(DateDiplome,'YYYYMM') )
);

CREATE TABLE Patients
(
  NrSIS       CHAR(11)
    CONSTRAINT PKPatients PRIMARY KEY,
  Nom         VARCHAR2(20)
    CONSTRAINT NNPatientsNom NOT NULL,
  Prenom      VARCHAR2(30)
    CONSTRAINT NNPatientsPrenom NOT NULL,
  Sexe        CHAR(1)
    CONSTRAINT NNPatientsSexe NOT NULL
    CONSTRAINT CKPatientsSexe CHECK ( Sexe in ('F', 'M') ),
  DateNais    DATE
    CONSTRAINT NNPatientsDateNais NOT NULL,
  Hospitalise VARCHAR2(1) DEFAULT 'N'
    CONSTRAINT CKPatientsHospitalise CHECK ( Hospitalise in ('O', 'N') ),
  CONSTRAINT CKPatientsNrSIS CHECK ( substr(NrSIS, 0, 6) = to_char(DateNais, 'YYMMDD'))

);

CREATE TABLE Services
(
  NumService     CHAR(4)
    CONSTRAINT PKServices PRIMARY KEY,
  NomService     VARCHAR2(20),
  NumChefService CHAR(10)
    CONSTRAINT NNServicesNumChefService NOT NULL
    CONSTRAINT FKMedecinsNumOrdre REFERENCES Medecins (NumOrdre) DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE Chambres
(
  NumService CHAR(4)
    CONSTRAINT FKServicesNumService REFERENCES Services (NumService),
  NumChambre NUMBER(4),
  CONSTRAINT PKChambres PRIMARY KEY (NumService, NumChambre)
);

CREATE TABLE PatientsChambres
(
  NrSIS      CHAR(11)
    CONSTRAINT FKPatientsNrSIS REFERENCES Patients (NrSIS) ON DELETE CASCADE ,
  DateEntree DATE,
  NumService CHAR(4),
  NumChambre NUMBER(4),
  DateSortie DATE,
  CONSTRAINT PKPatientsChambres PRIMARY KEY (NrSIS, DateEntree),
  CONSTRAINT FKChmabresServices FOREIGN KEY (NumService, NumChambre) REFERENCES Chambres (NumService, NumChambre),
  CONSTRAINT CKPatientsChambresDates CHECK ( DateEntree < DateSortie )
);

ALTER TABLE Medecins ADD CONSTRAINT FKMedecinsNumService FOREIGN KEY (NumService) REFERENCES Services(NumService);
