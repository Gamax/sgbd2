INSERT INTO SPECIALITES VALUES (0001,'Cardiologue');
INSERT INTO SPECIALITES VALUES (0002,'Pediatre');
INSERT INTO SPECIALITES VALUES (0003,'Neurologue');


-- insérer services puis medecins
INSERT INTO SERVICES VALUES ('C001','Cardiologie','19910342');
INSERT INTO SERVICES VALUES ('P001','Pediatrie','20021142');
INSERT INTO SERVICES VALUES ('N001','Neurologie','19870142');

INSERT INTO MEDECINS VALUES ('19910342','Schutz','Dylan',to_date('1991-03-21','YYYY-MM-DD'),0001,'C001');
INSERT INTO MEDECINS VALUES ('19850542','Schyns','Noé',to_date('1985-05-18','YYYY-MM-DD'),0001,'C001');
INSERT INTO MEDECINS VALUES ('20021142','Bemelmans','Stéphanie',to_date('2002-11-07','YYYY-MM-DD'),0002,'P001');
INSERT INTO MEDECINS VALUES ('20051242','Dethier','Thomas',to_date('2005-12-13','YYYY-MM-DD'),0002,'P001');
INSERT INTO MEDECINS VALUES ('19870142','Dussaussois','Florian',to_date('1987-01-05','YYYY-MM-DD'),0003,'N001');
INSERT INTO MEDECINS VALUES ('19930642','Gilson','Loic',to_date('1993-06-05','YYYY-MM-DD'),0003,'N001');

SET CONSTRAINT FKMedecinsNumOrdre IMMEDIATE;

INSERT INTO CHAMBRES VALUES ('C001',1);
INSERT INTO CHAMBRES VALUES ('C001',2);
INSERT INTO CHAMBRES VALUES ('C001',3);
INSERT INTO CHAMBRES VALUES ('C001',4);
INSERT INTO CHAMBRES VALUES ('P001',1);
INSERT INTO CHAMBRES VALUES ('P001',2);
INSERT INTO CHAMBRES VALUES ('P001',3);
INSERT INTO CHAMBRES VALUES ('P001',4);
INSERT INTO CHAMBRES VALUES ('P001',5);
INSERT INTO CHAMBRES VALUES ('N001',1);
INSERT INTO CHAMBRES VALUES ('N001',2);
INSERT INTO CHAMBRES VALUES ('N001',3);

INSERT INTO PATIENTS VALUES ('95101108','Gretry','Aurélien','M',to_date('11-10-95','DD-MM-YY'),'N');
INSERT INTO PATIENTS VALUES ('94060308','Crabbe','Pierre-Alexandre','M',to_date('03-06-94','DD-MM-YY'),'O');
INSERT INTO PATIENTS VALUES ('01102508','Tombeux','Julien','M',to_date('25-10-01','DD-MM-YY'),'O');
INSERT INTO PATIENTS VALUES ('06041608','Monsieur','Maëli','F',to_date('16-04-06','DD-MM-YY'),'N');
INSERT INTO PATIENTS VALUES ('98093008','Leon','Miguel','M',to_date('30-09-98','DD-MM-YY'),'O');

INSERT INTO PATIENTSCHAMBRES VALUES ('94060308',to_date('12-03-2019','DD-MM-YYYY'),'C001',3,NULL);
INSERT INTO PATIENTSCHAMBRES VALUES ('01102508',to_date('05-03-2019','DD-MM-YYYY'),'C001',2,NULL);
INSERT INTO PATIENTSCHAMBRES VALUES ('98093008',to_date('05-01-2019','DD-MM-YYYY'),'N001',1,NULL);
INSERT INTO PATIENTSCHAMBRES VALUES ('95101108',to_date('25-02-2019','DD-MM-YYYY'),'P001',1,to_date('05-03-2019','DD-MM-YYYY'));
INSERT INTO PATIENTSCHAMBRES VALUES ('06041608',to_date('06-05-2018','DD-MM-YYYY'),'P001',2,to_date('14-08-2018','DD-MM-YYYY'));

COMMIT ;