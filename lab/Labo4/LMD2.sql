-- ensemble
select nom, prenom
from Patients join Adresses using (codeAdresse)
where upper(EtatCivil) ='C'
  and upper(localite) = 'SAINT-NICOLAS'
  and extract (year from current_date) - extract (year from datenaissance) < 39

    MINUS

    select nom, prenom
    from Patients join PatientsMedecins using (NrSis)
    order by nom;



-- imbriquees

select Patients.Nom,Prenom
from Patients
         inner join Adresses using (codeAdresse)
where lower(EtatCivil) = 'c'
  and lower(localite) = 'saint-nicolas'
  and trunc(MONTHS_BETWEEN (current_date,PATIENTS.DateNaissance)/12) < 39
  and Patients.NrSis NOT IN (select NrSis
                             from Patients INNER JOIN PatientsMedecins USING (NrSis))
    order by 1;


-- correlee

select P1.Nom,Prenom
from Patients P1 INNER JOIN Adresses USING (CodeAdresse)
where lower(EtatCivil) ='c'
  and lower(localite) = 'saint-nicolas'
  and trunc(MONTHS_BETWEEN(current_date,P1.DateNaissance)/12) < 39
  and NOT EXISTS (SELECT *
                  from PatientsMedecins PM2
                  where P1.NrSis = PM2.NrSis)
    order by 1;

-- join

select Patients.nom,Prenom
from Patients  INNER JOIN Adresses USING (CodeAdresse)
               LEFT JOIN PatientsMedecins using (NrSis)
where lower(EtatCivil) ='c'
  and lower(localite) = 'saint-nicolas'
  and trunc(MONTHS_BETWEEN(current_date,PATIENTS.DateNaissance)/12) < 39
  and PatientsMedecins.NrMedecin IS NULL
    order by 1;
