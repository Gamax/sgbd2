-- ensemble

Select P.Nom AS Nom, P.Prenom AS Prenom
from Patients P
         INNER JOIN PatientsMedecins PM ON (P.NrSis = PM.NrSis)
         INNER JOIN Medecins M ON ( M.NrMedecin = PM.NrMedecin )
where Extract (YEAR from P.DateNaissance ) BETWEEN 1970 AND 1979
  and P.Sexe = 'F'
  and UPPER (M.Nom) = UPPER ('MUNNIX')
  and UPPER (M.Prenom) = UPPER('GEOFFROY')

    INTERSECT

    Select P.Nom AS Nom, P.Prenom AS Prenom
    from Patients P
    INNER JOIN PatientsMedecins PM ON (P.NrSis = PM.NrSis)
    INNER JOIN Medecins M ON ( M.NrMedecin = PM.NrMedecin )
    where Extract (YEAR from P.DateNaissance ) BETWEEN 1970 AND 1979
  and P.Sexe = 'F'
  and UPPER (M.Nom) = UPPER ('KAISON')
  and UPPER (M.Prenom) = UPPER('LALA')
    order by NOM;

-- imbriquee

Select P.Nom AS Nom, P.Prenom AS Prenom
from Patients P INNER JOIN PatientsMedecins PM ON P.NrSis = PM.NrSis)
    INNER JOIN Medecins M ON ( M.NrMedecin = PM.NrMedecin )
where Extract (YEAR from P.DateNaissance ) BETWEEN 1970 AND 1979
    and P.Sexe = 'F'
    and UPPER (M.Nom) = UPPER ('MUNNIX')
    and UPPER (M.Prenom) = UPPER('GEOFFROY')
    and P.NrSis IN (Select NrSis
    from PatientsMedecins INNER JOIN Medecins USING (NrMedecin)
    where UPPER (M.Nom) = UPPER ('KAISON')
    and UPPER (M.Prenom) = UPPER('LALA')
    order by Nom;

-- correlee

Select P.Nom AS Nom, P.Prenom AS Prenom
from Patients P
INNER JOIN PatientsMedecins PM ON (P.NrSis = PM.NrSis)
INNER JOIN Medecins M ON ( M.NrMedecin = PM.NrMedecin )
where Extract (YEAR from P.DateNaissance ) BETWEEN 1970 AND 1979
  and P.Sexe = 'F'
  and UPPER (M.Nom) = UPPER ('MUNNIX')
  and UPPER (M.Prenom) = UPPER('GEOFFROY')
  and EXISTS (Select *
              from PatientsMedecins INNER JOIN Medecins USING (NrMedecin)
              where UPPER (M.Nom) = UPPER ('KAISON')
                and UPPER (M.Prenom) = UPPER('LALA')
                and P.NrSis = PatientsMedecins.NrSis)
    order by Nom;