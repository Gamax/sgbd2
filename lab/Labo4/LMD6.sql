UPDATE Patients
set EtatCivil = 'M'
where NrSis = (Select NrSis
               from Allergies
               group by NrSis
               Having COUNT(*) = (Select MAX(COUNT(NrSis))
                                  from Allergies
                                           group by NrSis));

DELETE from PatientsMedecins
where NrSis = (Select NrSis
               from Allergies
               group by NrSis
               Having COUNT(*) = (Select MAX(COUNT(NrSis))
                                  from Allergies
                                  group by NrSis));


INSERT INTO PatientsMedecins
    (Select NrSis,NrMedecin
     from Patients P INNER JOIN ADRESSES AP ON (P.CodeAdresse = AP.CodeAdresse)
                     INNER JOIN Adresses AM ON (AP.CodePostal = AM.CodePostal)
                     INNER JOIN Medecins M ON (M.CodeAdresse = AM.CodeAdresse)
     where EXTRACT (YEAR From CURRENT_DATE ) - EXTRACT (YEAR FROM M.Datenaissance) > 55
       and NrSis = (Select NrSis
                    from Allergies
                    group by NrSis
                    Having COUNT(*) = (Select MAX(COUNT(NrSis))
                                       from Allergies
                                       group by NrSis)))