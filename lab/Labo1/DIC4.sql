SELECT OBJECT_TYPE                            AS "Type de l'objet",
       OBJECT_NAME                            AS "Nom de l'objet",
       to_char(CREATED, 'DD-MM-YYYY MI:HH24') AS "Date de création",
       CASE STATUS
         WHEN 'VALID'
           THEN 'YES'
         ELSE 'NO'
         END                                  AS "Statut"
FROM SYS.USER_OBJECTS
ORDER BY OBJECT_TYPE, OBJECT_NAME;