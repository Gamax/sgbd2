SELECT TABLE_NAME AS "Nom de la table",
       INDEX_NAME AS "Nom de l'index",
       CASE UNIQUENESS
         WHEN 'UNIQUE'
           THEN 'OUI'
         ELSE 'NON'
         END      AS "Unicité"
FROM USER_INDEXES
ORDER BY 1;