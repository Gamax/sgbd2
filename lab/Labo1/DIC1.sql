SELECT lower(username)                                                                   as "Nom de l'utilisateur",
       to_char(CREATED, 'DD-MM-YYYY MI:HH24')                                            as "Date de création",
       default_tablespace                                                                as "Tbl Sp Dflt",
       to_char(coalesce(expiry_date, created + interval '2' year), 'DD-MM-YYYY MI:HH24') as "Date d'expiration"
from SYS.USER_USERS;