SELECT STATUS, TIMESTAMP
FROM SYS.ALL_OBJECTS
WHERE OBJECT_NAME = 'LISTEEMPPRO';

ALTER TABLE EMPLOYES ADD "TEST" INT; --niveau logique

ALTER TABLE EMPLOYES DROP COLUMN TEST;

EXEC LISTEEMPPRO();

/*Invalide car on a modifié une table sur laquelle elle portait sans la réexécuter

Ensuite correct car on l'a réexecuté et elle s'est mis à jours avec la nouvelle colonne

Elle n'utilisait pa sla colonne ajoutée
*/