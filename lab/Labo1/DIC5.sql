SELECT COLUMN_NAME      AS "ColumnName",
       CONSTRAINT_NAME  AS "ConstraintName",
       CASE CONSTRAINT_TYPE
         WHEN 'C' THEN 'CHECK'
         WHEN 'P' THEN 'PRIMARY'
         WHEN 'R' THEN 'REFERENTIEL'
         WHEN 'U' THEN 'UNIQUE'
         END            AS "Type",
       CASE DEFERRED
         WHEN 'IMMEDIATE' THEN 'NO'
         WHEN 'DEFERRED' THEN 'YES'
         END            AS "Différée",
       STATUS           AS "Statut",
       SEARCH_CONDITION AS "La condition"
FROM USER_CONSTRAINTS
       NATURAL JOIN USER_CONS_COLUMNS
WHERE TABLE_NAME LIKE UPPER('&nom%')
ORDER BY COLUMN_NAME;