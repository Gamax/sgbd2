-- SET SERVEROUTPUT ON;

CREATE OR REPLACE PACKAGE GestionPatients IS
    procedure AjouterPatient (NewPatient In Patients %Rowtype);
    Procedure ModifierPatient (NewPatient In Patients%rowtype , OldPatient In Patients%rowtype);
    Procedure SupprimerPatient (Sis In Patients.NrSis %Type);
    FUNCTION Rechercher RETURN Medecins%ROWTYPE;
    FUNCTION RechercherLab8(LocLibelle IN Specialites.Libelle%TYPE DEFAULT 'MEDECINE GENERALE') RETURN Adresses.Localite%TYPE;
    FUNCTION Lister1 RETURN NUMBER;
    FUNCTION Lister2(NomMedic IN Medicaments.Denomination%TYPE) RETURN NUMBER;
END GestionPatients;

CREATE OR REPLACE PACKAGE BODY GestionPatients AS

    procedure AjouterPatient (NewPatient In Patients %Rowtype)
    As
        InvalidRefExc Exception;
        ApplicExc Exception;
        Pragma exception_init (InvalidRefExc , -2291);
        Pragma exception_init (ApplicExc , -2290);
        -- ajout catch unique constraint
    begin
        Insert into Patients values NewPatient;
        Commit;
    exception
        when InvalidRefExc then
            if(SQLERRM Like '%PAYS%') Then
                Raise_Application_Error (-20001, 'Le code pays ' || NewPatient.Nationalite || ' n''existe pas');
            Elsif(SQLERRM Like '%MUTUELLE%') Then
                Raise_Application_Error (-20002, 'Le code mutuelle ' || NewPatient.CodeMutuelle || ' n''existe pas');
            Elsif(SQLERRM Like '%ADRESSE%') Then
                Raise_Application_Error (-20003, 'L''adresse ' || NewPatient.CodeAdresse || ' n''existe pas');
            else
                DBMS_Output.Put_Line(SQLERRM);
            End if;

        When ApplicExc then
            if(SQLERRM Like '%CKPATIENTSEX%') then
                Raise_Application_Error(-20010, 'Le Sexe ' || NewPatient.Sexe || ' doit etre egal à M ou F');
            elsif (SQLERRM Like '%CKPATIENTETAT%') then
                Raise_Application_Error(-20011, 'L''etat Civil ' || NewPatient.EtatCivil || ' doit etre egal à C,M,D ou V');
            elsif (SQLERRM Like '%GRPSANG%') then
                Raise_Application_Error(-20012, 'Le groupe sanguin ' || NewPatient.GrpSanguin || ' doit etre egal à A,B,O ou AB');
            elsif (SQLERRM Like '%CPTBANCAIRE%') then
                Raise_Application_Error(-20013, 'Le compte bancaire est invalid ' || NewPatient.CptBancaire);
            elsif (SQLERRM Like '%NNPATIENTETATCI%') then
                Raise_Application_Error(-20014, 'L''etat civil ne doit pas etre null ' || NewPatient.EtatCivil);
            elsif (SQLERRM Like '%NNSEXEPAT%') then
                Raise_Application_Error(-20015, 'Le sexe ne doit pas etre null ' || NewPatient.Sexe);
            else
                DBMS_Output.Put_Line(SQLERRM);
            END IF;
        When others then raise;
    End Ajouterpatient;

    Procedure ModifierPatient (NewPatient In Patients%rowtype , OldPatient In Patients%rowtype)
    As
        InvalidRefExc Exception;
        ApplicExc Exception;
        BusyExc Exception;
        DiffExc Exception;
        Pragma exception_init (InvalidRefExc , -2291);
        Pragma exception_init (ApplicExc , -2290);
        Pragma exception_init (BusyExc , -54);
        CurrentTuple Patients%rowtype;
        Compteur NUMBER(1) := 0;
        Selection Number(1) := 1;

    Begin
        While Selection = 1 LOOP
            Begin
                Select * into CurrentTuple
                from Patients
                where NrSis = OldPatient.NrSis For update nowait;

                Selection := 0;

            Exception
                when BusyExc Then
                    if Compteur < 3 then
                        DBMS_LOCK.SLEEP(2);
                        Compteur := Compteur + 1;
                    else
                        raise;
                    end if;
            end;
        end loop;
        if(
                    COALESCE ( OldPatient.Nom ,'NULL') <> COALESCE ( CurrentTuple.Nom ,'NULL')
                OR COALESCE ( OldPatient.Prenom ,'NULL') <> COALESCE ( CurrentTuple.Prenom ,'NULL')
                OR OldPatient.Sexe <> CurrentTuple.sexe
                OR OldPatient.EtatCivil <> CurrentTuple.EtatCivil
                OR COALESCE ( OldPatient.Nationalite ,'NULL') <> COALESCE ( CurrentTuple.Nationalite ,'NULL')
                OR trunc(OldPatient.Datenaissance) <> trunc(CurrentTuple.Datenaissance)
                OR COALESCE ( OldPatient.CptBancaire ,'NULL') <> COALESCE ( CurrentTuple.CptBancaire ,'NULL')
                OR COALESCE ( OldPatient.GrpSanguin ,'NULL') <> COALESCE ( CurrentTuple.GrpSanguin ,'NULL')
                OR OldPatient.Taille <> CurrentTuple.Taille
                OR OldPatient.Poids  <> CurrentTuple.Poids
                OR COALESCE ( OldPatient.CodeMutuelle ,'NULL') <> COALESCE ( CurrentTuple.CodeMutuelle ,'NULL')
                OR OldPatient.CodeAdresse <> CurrentTuple.CodeAdresse)
        then
            Raise DiffExc;
        end if;
        UPDATE Patients
        Set row = NewPatient
        where NrSis = OldPatient.NrSis;

        Commit;

    Exception
        When BusyExc Then
            Raise_Application_Error(-20010, 'Le tuple ' || OldPatient.NrSis || ' est occupé');
        WHen NO_DATA_FOUND Then
            Raise_Application_Error(-20011, 'Le tuple ' || OldPatient.NrSis || ' n''existe pas');
        When DiffExc Then
            Raise_Application_Error(-20012, 'Le tuple ' || OldPatient.NrSis || ' est different du Patient qu''on veut modifier');

        when InvalidRefExc then
            if(SQLERRM Like '%PAYS%') Then
                Raise_Application_Error (-20001, 'Le code pays ' || NewPatient.Nationalite || ' n''existe pas');
            Elsif(SQLERRM Like '%MUTUELLE%') Then
                Raise_Application_Error (-20002, 'Le code mutuelle ' || NewPatient.CodeMutuelle || ' n''existe pas');
            Elsif(SQLERRM Like '%ADRESSE%') Then
                Raise_Application_Error (-20003, 'L''adresse ' || NewPatient.CodeAdresse || ' n''existe pas');
            else
                DBMS_Output.Put_Line(SQLERRM);
            End if;

        When ApplicExc then
            if(SQLERRM Like '%CKPATIENTSEX%') then
                Raise_Application_Error(-20020, 'Le Sexe ' || NewPatient.Sexe || ' doit etre egal à M ou F');
            elsif (SQLERRM Like '%CKPATIENTETAT%') then
                Raise_Application_Error(-20021, 'L''etat Civil ' || NewPatient.EtatCivil || ' doit etre egal à C,M,D ou V');
            elsif (SQLERRM Like '%GRPSANG%') then
                Raise_Application_Error(-20022, 'Le groupe sanguin ' || NewPatient.GrpSanguin || ' doit etre egal à A,B,O ou AB');
            elsif (SQLERRM Like '%CPTBANCAIRE%') then
                Raise_Application_Error(-20023, 'Le compte bancaire est invalid ' || NewPatient.CptBancaire);
            elsif (SQLERRM Like '%NNPATIENTETATCI%') then
                Raise_Application_Error(-20024, 'L''etat civil ne doit pas etre null ' || NewPatient.EtatCivil);
            elsif (SQLERRM Like '%NNSEXEPAT%') then
                Raise_Application_Error(-20025, 'Le sexe ne doit pas etre null ' || NewPatient.Sexe);
            else
                DBMS_Output.Put_Line(SQLERRM);
            END IF;
        When Others then
            Raise;
    End;

    Procedure SupprimerPatient (Sis In Patients.NrSis %Type)
    As
        InvalidRefExc Exception;
        NullParamExc Exception;
        MedGenExc Exception;
        ForeignKeyExc Exception;
        Pragma exception_init (ForeignKeyExc , -2292);
        adressePatient Patients.CodeAdresse %TYPE;
    BEGIN
        IF Sis IS NULL THEN
            RAISE NullParamExc;
        END IF;

        SELECT CodeAdresse INTO adressePatient
        FROM Patients
        WHERE NrSis = Sis;

        DELETE FROM Patients
        Where NrSis = Sis
          AND NrSis NOT IN (  SELECT pm.NrSis
                              FROM Patientsmedecins pm, Medecins m, Specialites s
                              WHERE pm.NrMedecin = m.NrMedecin
                                AND m.Specialite = s.Specialite
                                AND s.Libelle = 'MEDECINE GENERALE');
        IF SQL%NOTFOUND THEN
            RAISE MedGenExc;
        END IF;

        BEGIN
            DELETE FROM adresses
            WHERE CodeAdresse = adressePatient;
        EXCEPTION
            WHEN ForeignKeyExc THEN
                Raise_Application_Error (-20001, 'Le code adresse ' || adressePatient || ' est utilisé dans d''autre tuples');
            WHEN OTHERS THEN
                RAISE;
        END;

    Exception
        WHen NO_DATA_FOUND Then
            Raise_Application_Error(-20011, 'Le tuple ' || Sis || ' n''existe pas');
        WHen MedGenExc Then
            Raise_Application_Error(-20012, 'Le tuple ' || Sis || ' a un medecin generaliste');
        WHen NullParamExc Then
            Raise_Application_Error(-20013, 'Le parametre de la procedure est nul !');
        When OTHERS Then
            RAISE ;

    END;

    FUNCTION Rechercher RETURN Medecins%ROWTYPE
    AS
        UnMedecin Medecins%ROWTYPE;
    BEGIN
        SELECT M.*  INTO UnMedecin
        FROM MEDECINS M
                 INNER JOIN PATIENTSMEDECINS PM ON PM.NrMedecin = M.NrMedecin
                 INNER JOIN PATIENTS P ON PM.NrSis = P.NrSis
                 INNER JOIN ADRESSES A ON A.CodeAdresse = P.CodeAdresse
        WHERE UPPER(A.LOCALITE) = UPPER('Saint-Nicolas')
            INTERSECT
            SELECT M.*
            FROM MEDECINS M
                  INNER JOIN PATIENTSMEDECINS PM ON PM.NrMedecin = M.NrMedecin
                  INNER JOIN PATIENTS P ON PM.NrSis = P.NrSis
                  INNER JOIN ADRESSES A ON A.CodeAdresse = P.CodeAdresse
            WHERE UPPER(A.LOCALITE) = UPPER('Liege');

        RETURN UnMedecin;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20200, 'Aucun medecin trouve.');
        WHEN TOO_MANY_ROWS THEN
            RAISE_APPLICATION_ERROR(-20201, 'Plusieurs medecins trouves.');
        WHEN OTHERS THEN RAISE;
    END Rechercher;

    FUNCTION RechercherLab8(LocLibelle IN Specialites.Libelle%TYPE DEFAULT 'MEDECINE GENERALE')
        RETURN Adresses.Localite%TYPE
    AS
        ResLocalite Adresses.Localite%TYPE;
    BEGIN

        SELECT Localite INTO ResLocalite
        FROM Adresses
                 INNER JOIN Medecins USING(CodeAdresse)
                 INNER JOIN Specialites USING(Specialite)
        WHERE UPPER(Libelle) = 'MEDECINE GENERALE'
            GROUP BY Localite
            HAVING COUNT(*) = (SELECT MAX(COUNT(*))                       --
                               FROM MEDECINS
                                        INNER JOIN Specialites USING(Specialite)
                                        INNER JOIN Adresses USING(CodeAdresse)
                               WHERE UPPER(Libelle) = LocLibelle
                                   GROUP BY Localite);

        RETURN ResLocalite;
    EXCEPTION
        WHEN TOO_MANY_ROWS THEN
            RAISE_APPLICATION_ERROR(-20031, 'Trop de localites pour la specialite ' || LocLibelle);
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20032, 'Pas de localite pour la specialite ' || LocLibelle);
        WHEN OTHERS THEN RAISE;

    END RechercherLab8;

    FUNCTION Lister1
        RETURN NUMBER
    AS
        CURSOR lesLocalites IS
            SELECT DISTINCT(A.LOCALITE)
            FROM ADRESSES A
            WHERE NOT EXISTS (
                    SELECT *
                    FROM PATIENTS P
                             INNER JOIN ADRESSES AP ON AP.CODEADRESSE = P.CODEADRESSE
                    WHERE A.LOCALITE = AP.LOCALITE)
              AND EXISTS(
                    SELECT *
                    FROM MUTUELLES M
                             INNER JOIN ADRESSES AM ON AM.CODEADRESSE = M.CODEADRESSE
                    WHERE A.LOCALITE = AM.LOCALITE)
                ORDER BY 1;

        UneLocalite Adresses.Localite%TYPE;
        NbFound NUMBER;
    BEGIN
        NbFound := 0;

        OPEN lesLocalites;
        FETCH lesLocalites INTO UneLocalite;
        WHILE lesLocalites%FOUND
            LOOP
                DBMS_OUTPUT.PUT_LINE(UneLocalite);
                FETCH lesLocalites INTO UneLocalite;
            END LOOP;

        --IF lesLocalites%ROWCOUNT = 0 THEN RAISE ExNoData;
        --END IF;

        NbFound := lesLocalites%ROWCOUNT;

        CLOSE lesLocalites;

        RETURN NbFound;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20300, 'Pas de localite');
        WHEN OTHERS THEN
            IF lesLocalites%ISOPEN THEN
                CLOSE lesLocalites;
            END IF;
            RAISE;

    END;

    FUNCTION Lister2(NomMedic IN Medicaments.Denomination%TYPE)
        RETURN NUMBER
        IS
        CURSOR LesPatients(nomMedic IN Medicaments.Denomination%TYPE) IS
            SELECT DISTINCT P.Nom, P.Prenom, S.Nom
            FROM PATIENTS P
                     INNER JOIN ALLERGIES A ON P.NRSIS = A.NRSIS
                     INNER JOIN SUBSTANCES S ON A.NRSUBSTANCE = S.NRSUBSTANCE
                     INNER JOIN COMPOSITION C ON S.NRSUBSTANCE = C.NRSUBSTANCE
                     INNER JOIN MEDICAMENTS M ON C.NRMEDICAMENT = M.NRMEDICAMENT
            WHERE M.DENOMINATION != nomMedic
                ORDER BY 1, 2;

        NbFound NUMBER;

        NomPatient Patients.Nom%TYPE;
        PrenomPatient Patients.Prenom%TYPE;
        NomSubstance Substances.Nom%TYPE;
    BEGIN
        NbFound := 0;

        OPEN lesPatients(NomMedic);
        FETCH lesPatients INTO NomPatient, PrenomPatient, NomSubstance;
        WHILE lesPatients%FOUND
            LOOP
                DBMS_OUTPUT.PUT_LINE(NomPatient || ' ' || PrenomPatient ||
                                     ' - ' || NomSubstance);
                FETCH lesPatients INTO NomPatient, PrenomPatient, NomSubstance;
            END LOOP;

        NbFound := lesPatients%ROWCOUNT;

        CLOSE lesPatients;
        RETURN NbFound;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20300, 'Pas de patient.');
        WHEN OTHERS THEN
            IF lesPatients%ISOPEN THEN
                CLOSE lesPatients;
            END IF;
            RAISE;

    END Lister2;

END GestionPatients;