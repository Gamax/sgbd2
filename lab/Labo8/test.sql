-- test ajouter
DECLARE
    UnPatient Patients%ROWTYPE;
BEGIN
    UnPatient.NrSis := 'xxx';
    UnPatient.Nom := 'Dethier';
    UnPatient.Prenom := 'Thomas';
    UnPatient.Sexe := 'D';
    UnPatient.EtatCivil := 'M';
    UnPatient.Nationalite := 'BE';
    UnPatient.DateNaissance := TO_DATE('14/01/97', 'DD/MM/RR');

    GESTIONPATIENTS.AjouterPatient(UnPatient);

    DBMS_OUTPUT.PUT_LINE('Ajout effectue');
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

-- test lister1
DECLARE
    NbRow NUMBER;
BEGIN
    NbRow := GESTIONPATIENTS.LISTER1();
    DBMS_OUTPUT.PUT_LINE('Nb localite : ' || NbRow);
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

-- test lister2
DECLARE
    NbRow NUMBER;
BEGIN
    NbRow := GESTIONPATIENTS.lister2('PENI-ORAL');
    DBMS_OUTPUT.PUT_LINE('Nb patient : ' || NbRow);
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;