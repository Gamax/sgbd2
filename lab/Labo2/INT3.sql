begin
  gestionprojets.supprimerprojet
    ('p10345');
  dbms_output.put_line('Projet supprimé');
Exception
  when others then dbms_output.put_line(sqlerrm);
end;

-- delete projet alors qu'il y a encore des employes dessus