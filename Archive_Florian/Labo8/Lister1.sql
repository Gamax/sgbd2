/*CREATE OR REPLACE PACKAGE GestionPatients IS
    FUNCTION Lister RETURN NUMBER;
END GestionPatients;

CREATE OR REPLACE PACKAGE BODY GestionPatients AS  
    --ExNoData EXCEPTION;
    --PRAGMA EXCEPTION_INIT(ExNoData, -1403);*/
CREATE OR REPLACE FUNCTION ListerLoc
    RETURN NUMBER
AS
    CURSOR lesLocalites IS
        SELECT DISTINCT(A.LOCALITE)
        FROM ADRESSES A
        WHERE NOT EXISTS (
            SELECT *
                FROM PATIENTS P
                    INNER JOIN ADRESSES AP ON AP.CODEADRESSE = P.CODEADRESSE
                WHERE A.LOCALITE = AP.LOCALITE)
        AND EXISTS(
            SELECT *
                FROM MUTUELLES M
                    INNER JOIN ADRESSES AM ON AM.CODEADRESSE = M.CODEADRESSE
                WHERE A.LOCALITE = AM.LOCALITE)
        ORDER BY 1;
        
    UneLocalite Adresses.Localite%TYPE;
    NbFound NUMBER;
BEGIN
    NbFound := 0;
    
    OPEN lesLocalites;
    FETCH lesLocalites INTO UneLocalite;
    WHILE lesLocalites%FOUND
    LOOP
        DBMS_OUTPUT.PUT_LINE(UneLocalite);
        FETCH lesLocalites INTO UneLocalite;
    END LOOP;
    
    --IF lesLocalites%ROWCOUNT = 0 THEN RAISE ExNoData;
    --END IF;
    
    NbFound := lesLocalites%ROWCOUNT;
    
    CLOSE lesLocalites;
    
    RETURN NbFound;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20300, 'Pas de localite');
    WHEN OTHERS THEN
        IF lesLocalites%ISOPEN THEN
            CLOSE lesLocalites;
        END IF;
        RAISE;

END ListerLoc;
--END GestionPatients;

DECLARE
    NbRow NUMBER;
BEGIN
    NbRow := ListerLoc();
    DBMS_OUTPUT.PUT_LINE('Nb localite : ' || NbRow);
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

SET SERVEROUTPUT ON;
-- OU Requete ensembliste Minus

SELECT DISTINCT A.Localite
FORM Adresses A LEFT JOIN Patients P ON A.CodeAdresse = P.CodeAdresse
    INNER JOIN Mutuelles M ON
