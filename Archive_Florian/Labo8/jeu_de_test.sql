SET SERVEROUTPUT ON;
DECLARE 
	Resulats GestionPatients.ListerNomPrenomSubstance;
BEGIN
	Resulats := GestionPatients.Lister('DICHLOTRIDE');
	FOR i IN 1 .. Resulats.COUNT LOOP
		DBMS_OUTPUT.PUT_LINE(Resulats(i).Nom || ' ' || Resulats(i).Prenom || ' ' || Resulats(i).Substance);
	END LOOP;
EXCEPTION
	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE (SQLEERM)