CREATE OR REPLACE PACKAGE GestionPatients IS
    PROCEDURE AjouterPatient (UnPatient IN Patients%ROWTYPE);
    PROCEDURE ModifierPatient (OldPatient Patients%ROWTYPE, NewPatient Patients%ROWTYPE);
    PROCEDURE SupprimerPatient (MyNrSis Patients.NrSis%TYPE);
    FUNCTION Rechercher RETURN Medecins%ROWTYPE ;
END GestionPatients;

CREATE OR REPLACE PACKAGE BODY GestionPatients AS  
PROCEDURE AjouterPatient (UnPatient IN Patients%ROWTYPE)
AS
    InvalidReference EXCEPTION;
    PRAGMA EXCEPTION_INIT(InvalidReference, -2291);
    
    --NullDetected EXCEPTION;
    --PRAGMA EXCEPTION_INIT(NullDetected, -1400); Deja defini dans contrainte applicative (mis dans le check)
    
    ApplicConstr EXCEPTION;
    PRAGMA EXCEPTION_INIT(ApplicConstr,-2290);
BEGIN
    INSERT INTO PATIENTS VALUES UnPatient;
    COMMIT; -- Attetion commit !
EXCEPTION
    WHEN InvalidReference THEN
        --IF(INSTR(SQLERRM, 'REFPATIENTSADRESSES') >= 1) THEN RAISE_APP...
        IF(SQLERRM LIKE '%PAYS%') THEN RAISE_APPLICATION_ERROR
            (-20010, 'La nationalite '|| UnPatient.Nationalite || ' est erronee');
        ELSIF(SQLERRM LIKE '%MUTUELLE%') THEN RAISE_APPLICATION_ERROR
            (-20011, 'Le code mutuelle ' || UnPatient.CodeMutuelle || ' est errone');
        ELSIF (SQLERRM LIKE '%ADRESSE%') THEN RAISE_APPLICATION_ERROR
            (-20012, 'Le code adresse ' || UnPatient.CodeAdresse || ' est errone');
        -- ELSE RAISE; Prof na pas mit
        END IF;
    
    WHEN ApplicConstr THEN
        -- Ou IF(INSTR(SQLERRM, 'PATIENTSSEXE') >= 1)
        IF(SQLERRM LIKE '%SEXE%') THEN RAISE_APPLICATION_ERROR(-20020,
            'Le code sexe est invalide '|| UnPatient.Sexe || ' - (F ou M) attendu');
        ELSIF(SQLERRM LIKE '%ETATCIVIL%') THEN RAISE_APPLICATION_ERROR(-20021,
            'Le code etat civil est invalide ' || UnPatient.Etatcivil || ' - (C, M, D, V) attendu');
         ELSIF(SQLERRM LIKE '%GRPSANGUIN%') THEN RAISE_APPLICATION_ERROR(-20022,
            'Le code groupe sanguin est invalide ' || UnPatient.Grpsanguin || ' - (A, B, O, AB) attendu');
         ELSIF(SQLERRM LIKE '%CPTBANCAIRE%') THEN RAISE_APPLICATION_ERROR(-20023,
            'Le code etat civil est invalide ' || UnPatient.Cptbancaire);
            ELSIF(SQLERRM LIKE 'PATIENTETATCIVILNOTNULL') THEN RAISE_APPLICATION_ERROR(-20024,
            'Le code etat civil ne doit pas etre null : ' || UnPatient.Etatcivil || ' - (C, M, D, V) attendu');
        END IF;
    WHEN OTHERS THEN RAISE;

END AjouterPatient;

PROCEDURE ModifierPatient (OldPatient Patients%ROWTYPE, NewPatient Patients%ROWTYPE)
AS
    ExCleEtrangere EXCEPTION;
    PRAGMA EXCEPTION_INIT(ExCleEtrangere, -2291);
    
    ExApplicative EXCEPTION;
    PRAGMA EXCEPTION_INIT(ExApplicative,-2290);
    
    ExEntite EXCEPTION;
    PRAGMA EXCEPTION_INIT(ExEntite, -1);
    
BEGIN

    UPDATE PATIENTS 
        SET -- Avec le declencheur
            -- Date de naissance modifier donc nrsis modifier 
            --  et mis a jour nrsis sur les autres tables
            Nom = NewPatient.Nom,
            Prenom = NewPatient.Prenom,
            Sexe = NewPatient.Sexe,
            EtatCivil = NewPatient.EtatCivil,
            Nationalite = NewPatient.Nationalite,
            CptBancaire = NewPatient.CptBancaire,
            GrpSanguin = NewPatient.GrpSanguin,
            Taille = NewPatient.Taille,
            Poids = NewPatient.Poids,
            CodeMutuelle = NewPatient.CodeMutuelle,
            CodeAdresse = NewPatient.CodeAdresse
        WHERE OldPatient.NrSis = NrSis;

    -- NrSis modifie par Declencheur
    IF(OldPatient.DateNaissance != NewPatient.DateNaissance) THEN
        UPDATE PATIENTS SET DateNaissance = NewPatient.DateNaissance
            WHERE OldPatient.NrSis = NrSis;
    END IF;
    
    COMMIT;
EXCEPTION
    WHEN ExCleEtrangere THEN
        IF(SQLERRM LIKE '%PAYS%') THEN RAISE_APPLICATION_ERROR
            (-20010, 'La nationalite '|| NewPatient.Nationalite || ' est erronee');
        ELSIF(SQLERRM LIKE '%MUTUELLE%') THEN RAISE_APPLICATION_ERROR
            (-20011, 'Le code mutuelle ' || NewPatient.CodeMutuelle || ' est errone');
        ELSIF (SQLERRM LIKE '%ADRESSE%') THEN RAISE_APPLICATION_ERROR
            (-20012, 'Le code adresse ' || NewPatient.CodeAdresse || ' est errone');
        ELSE RAISE;
        END IF;
    
    WHEN ExApplicative THEN
        IF(SQLERRM LIKE '%SEXE%') THEN RAISE_APPLICATION_ERROR(-20020,
            'Le code sexe est invalide '|| NewPatient.Sexe || ' - (F ou M) attendu');
        ELSIF(SQLERRM LIKE '%ETATCIVIL%') THEN RAISE_APPLICATION_ERROR(-20021,
            'Le code etat civil est invalide ' || NewPatient.Etatcivil || ' - (C, M, D, V) attendu');
         ELSIF(SQLERRM LIKE '%GRPSANGUIN%') THEN RAISE_APPLICATION_ERROR(-20022,
            'Le code groupe sanguin est invalide ' || NewPatient.Grpsanguin || ' - (A, B, O, AB) attendu');
         ELSIF(SQLERRM LIKE '%CPTBANCAIRE%') THEN RAISE_APPLICATION_ERROR(-20023,
            'Le code etat civil est invalide ' || NewPatient.Cptbancaire);
        ELSE RAISE;
        END IF;
        
    --WHEN ExEntite THEN RAISE_APPLICATION_ERROR(-20024, 'NrSis deja existant');
    WHEN OTHERS THEN RAISE;
END ModifierPatient;

PROCEDURE SupprimerPatient (MyNrSis Patients.NrSis%TYPE)
AS
    NbMedecinGe number;
    
    NbAdresse Number;
    MaxAdresse Number;
    MyCodeAdresse PATIENTS.CodeAdresse%TYPE;
    
    ExceptionMG EXCEPTION;
    -- Ne pas effectuer tout les tests avant le delete. Tester dans le delete
BEGIN
    -- Verifier si NrSis null

    SELECT P.CodeAdresse INTO MyCodeAdresse FROM PATIENTS P WHERE P.NrSis = MyNrSis;
    
    -- Verif medecin generaliste
    SELECT Count(*) INTO NbMedecinGe
        FROM PATIENTS P INNER JOIN PATIENTSMEDECINS PM ON P.NrSis = PM.NrSis
            INNER JOIN MEDECINS M ON M.NrMedecin = PM.NrMedecin
            INNER JOIN SPECIALITES S ON S.Specialite = M.Specialite
        WHERE UPPER(S.Libelle) = UPPER('Medecine Generale')
            AND P.NrSis = MyNrSis;
            
    IF(NbMedecinGe = 0) THEN
        DELETE PATIENTS WHERE NrSis = MyNrSis;
        -- Verif adresse
    
        SELECT COUNT(*) INTO NbAdresse
        FROM ADRESSES A INNER JOIN PATIENTS P ON A.CodeAdresse = P.CodeAdresse
        WHERE A.CodeAdresse = MyCodeAdresse;
        MaxAdresse := NbAdresse;
        
        SELECT COUNT(*) INTO NbAdresse
        FROM ADRESSES A INNER JOIN MEDECINS M ON A.CodeAdresse = M.CodeAdresse
        WHERE A.CodeAdresse = MyCodeAdresse;
        MaxAdresse := MaxAdresse + NbAdresse;
        
        MaxAdresse := MaxAdresse -1;
        
        IF(MaxAdresse = 0) THEN
            DELETE ADRESSES WHERE CODEADRESSE = MyCodeAdresse;
        END IF;
    ELSE
        RAISE EXCEPTIONMG;
    END IF;
   
   COMMIT;
EXCEPTION
    WHEN EXCEPTIONMG THEN RAISE_APPLICATION_ERROR(-20030,
            'Le patient a supprimer ne doit pas avoir de medecin generaliste');
    WHEN OTHERS THEN RAISE;
END SupprimerPatient;

FUNCTION Rechercher
    RETURN Medecins%ROWTYPE 
    AS
    UnMedecin Medecins%ROWTYPE;
BEGIN
    SELECT M.*  INTO UnMedecin
    FROM MEDECINS M 
        INNER JOIN PATIENTSMEDECINS PM ON PM.NrMedecin = M.NrMedecin
        INNER JOIN PATIENTS P ON PM.NrSis = P.NrSis
        INNER JOIN ADRESSES A ON A.CodeAdresse = P.CodeAdresse
    WHERE UPPER(A.LOCALITE) = UPPER('Saint-Nicolas')
INTERSECT
    SELECT M.*
    FROM MEDECINS M 
        INNER JOIN PATIENTSMEDECINS PM ON PM.NrMedecin = M.NrMedecin
        INNER JOIN PATIENTS P ON PM.NrSis = P.NrSis
        INNER JOIN ADRESSES A ON A.CodeAdresse = P.CodeAdresse
    WHERE UPPER(A.LOCALITE) = UPPER('Liege');
    
    RETURN UnMedecin;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20200, 'Aucun medecin trouve.');
    WHEN TOO_MANY_ROWS THEN
        RAISE_APPLICATION_ERROR(-20201, 'Plusieurs medecins trouves.');
    WHEN OTHERS THEN RAISE;
END Rechercher;

END GestionPatients;
