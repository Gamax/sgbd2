/*CREATE OR REPLACE PACKAGE GestionPatients IS
    FUNCTION Lister2(NomMedic IN Medicaments.Denomination%TYPE) RETURN NUMBER;
END GestionPatients;

CREATE OR REPLACE PACKAGE BODY GestionPatients AS  
    ExNoData EXCEPTION;
    --PRAGMA EXCEPTION_INIT(ExNoData, -1403); */
    SET SERVEROUTPUT ON;
CREATE OR REPLACE FUNCTION Lister2(NomMedic IN Medicaments.Denomination%TYPE)
    RETURN NUMBER
IS
    CURSOR LesPatients(nomMedic IN Medicaments.Denomination%TYPE) IS
        SELECT DISTINCT P.Nom, P.Prenom, S.Nom
            FROM PATIENTS P
                INNER JOIN ALLERGIES A ON P.NRSIS = A.NRSIS
                INNER JOIN SUBSTANCES S ON A.NRSUBSTANCE = S.NRSUBSTANCE
                INNER JOIN COMPOSITION C ON S.NRSUBSTANCE = C.NRSUBSTANCE
                INNER JOIN MEDICAMENTS M ON C.NRMEDICAMENT = M.NRMEDICAMENT
            WHERE M.DENOMINATION != nomMedic
            ORDER BY 1, 2;
        
    NbFound NUMBER;
    
    NomPatient Patients.Nom%TYPE;
    PrenomPatient Patients.Prenom%TYPE;
    NomSubstance Substances.Nom%TYPE;
BEGIN
    NbFound := 0;
    
    OPEN lesPatients(NomMedic);
    FETCH lesPatients INTO NomPatient, PrenomPatient, NomSubstance;
    WHILE lesPatients%FOUND
    LOOP
        DBMS_OUTPUT.PUT_LINE(NomPatient || ' ' || PrenomPatient || 
                                ' - ' || NomSubstance);
        FETCH lesPatients INTO NomPatient, PrenomPatient, NomSubstance;
    END LOOP;
    
    /*IF lesPatients%ROWCOUNT = 0 THEN RAISE ExNoData;
    END IF;*/
    
    NbFound := lesPatients%ROWCOUNT;
    
    CLOSE lesPatients;
    RETURN NbFound;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20300, 'Pas de patient.');
    WHEN OTHERS THEN
        IF lesPatients%ISOPEN THEN
            CLOSE lesPatients;
        END IF;
        RAISE;

END Lister2;
--END GestionPatients;

DECLARE
    NbRow NUMBER;
BEGIN
    NbRow := lister2('DAFALGAN CODEINE');
    DBMS_OUTPUT.PUT_LINE('Nb patient : ' || NbRow);
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
    
SELECT * FROM MEDICAMENTS;

-- Test
DECLARE
    Resultats GestionPatients.ListerNomPrenomSubstance;
BEGIN
    Resultats := GestionPatients.Lister('DAFALGAN CODEINE');
    
    FOR i IN 1 .. Resultats.COUNT LOOP
        DBMS_OUTPUT.PUT_LINE(Resultats(i).Nom || ' ' || Resultats(i).Prenom || ' - ' ||
                             Resultats(i).Substance);
    END LOOP;
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;



-- La requete a mettre pour l'examen dans la fonction lister 
SELECT pa.Nom AS "Nom",
    pa.Prenom AS "Prenom"
FROM PATIENTS pa
        INNER JOIN PATIENTSMEDECINS pm ON pa.NRSIS = pm.NRSIS
        INNER JOIN PATIENTSMEDECINS pm2 ON pa.NRSIS = pm2.NRSIS
        INNER JOIN MEDECINS med  ON med.NRMEDECIN = pm.NRMEDECIN
        INNER JOIN MEDECINS med2 ON med2.NRMEDECIN = pm2.NRMEDECIN
    WHERE 
        EXTRACT(YEAR FROM pa.DateNaissance) BETWEEN 1970 AND 1979
        AND med2.NRMEDECIN <> med.NRMEDECIN
        AND pa.SEXE = 'F'
        AND (UPPER(med.NOM) LIKE UPPER('Munnix') AND UPPER(med.PRENOM) LIKE UPPER('Geoffroy'))
        AND (UPPER(med2.NOM) LIKE UPPER('Kaison') AND UPPER(med2.PRENOM) LIKE UPPER('Lala'))
        
        ORDER BY 1;
