-- Trigger 1
CREATE OR REPLACE TRIGGER PatientsInsertNrSis -- cote serveur
BEFORE INSERT ON Patients
FOR EACH ROW
DECLARE
    EXCEPTIONDN EXCEPTION;
BEGIN
    IF(:NEW.DateNaissance IS NULL) THEN
        RAISE EXCEPTIONDN;
    END IF;
    
   SELECT TO_CHAR(:NEW.datenaissance , 'YYMMDD') ||
    COALESCE
        (LPAD(TO_CHAR(MAX(TO_NUMBER(SUBSTR(NRSIS, 7)) +1)), 5, '0'), 
        '00001')
    INTO :NEW.nrsis FROM patients;
EXCEPTION
    WHEN ExceptionDN THEN raise_application_Error(-20001, 'Date de naissance ne peut pas etre null.');
    WHEN OTHERS THEN RAISE;
END;
SHOW ERRORS TRIGGER PatientsInsertNrSis;

-- Dup val on index exception si cle primaire mauvais ou un champs unique deja existant
-- A chaque contrainte UNIQUE il y  a un index
-- Tout les objets dans la DB sont en majuscules

-- Pour mettre un apostrophe : le mettre 2 fois ''

-- Grace au trigger, on n'aura jamais de contrainte d'entite
-- Pour l'ajout
-- Dans une procedure, on ne verifie aucune donnee... Le faire dans les contraintes pour verifier si null
-- Pas de verif pour l'ajout !!
-- Procedure Ajouter patient
CREATE OR REPLACE PROCEDURE
    AjouterPatient (UnPatient IN Patients%ROWTYPE)
AS
    InvalidReference EXCEPTION;
    PRAGMA EXCEPTION_INIT(InvalidReference, -2291);
    
    --NullDetected EXCEPTION;
    --PRAGMA EXCEPTION_INIT(NullDetected, -1400); Deja defini dans contrainte applicative (mis dans le check)
    
    ApplicConstr EXCEPTION;
    PRAGMA EXCEPTION_INIT(ApplicConstr,-2290);
BEGIN
    INSERT INTO PATIENTS VALUES UnPatient;
    COMMIT; -- Attetion commit !
EXCEPTION
    WHEN InvalidReference THEN
        --IF(INSTR(SQLERRM, 'REFPATIENTSADRESSES') >= 1) THEN RAISE_APP...
        IF(SQLERRM LIKE '%PAYS%') THEN RAISE_APPLICATION_ERROR
            (-20010, 'La nationalite '|| UnPatient.Nationalite || ' est erronee');
        ELSIF(SQLERRM LIKE '%MUTUELLE%') THEN RAISE_APPLICATION_ERROR
            (-20011, 'Le code mutuelle ' || UnPatient.CodeMutuelle || ' est errone');
        ELSIF (SQLERRM LIKE '%ADRESSE%') THEN RAISE_APPLICATION_ERROR
            (-20012, 'Le code adresse ' || UnPatient.CodeAdresse || ' est errone');
        -- ELSE RAISE; Prof na pas mit
        END IF;
    
    WHEN ApplicConstr THEN
        -- Ou IF(INSTR(SQLERRM, 'PATIENTSSEXE') >= 1)
        IF(SQLERRM LIKE '%SEXE%') THEN RAISE_APPLICATION_ERROR(-20020,
            'Le code sexe est invalide '|| UnPatient.Sexe || ' - (F ou M) attendu');
        ELSIF(SQLERRM LIKE '%ETATCIVIL%') THEN RAISE_APPLICATION_ERROR(-20021,
            'Le code etat civil est invalide ' || UnPatient.Etatcivil || ' - (C, M, D, V) attendu');
         ELSIF(SQLERRM LIKE '%GRPSANGUIN%') THEN RAISE_APPLICATION_ERROR(-20022,
            'Le code groupe sanguin est invalide ' || UnPatient.Grpsanguin || ' - (A, B, O, AB) attendu');
         ELSIF(SQLERRM LIKE '%CPTBANCAIRE%') THEN RAISE_APPLICATION_ERROR(-20023,
            'Le code etat civil est invalide ' || UnPatient.Cptbancaire);
            ELSIF(SQLERRM LIKE 'PATIENTETATCIVILNOTNULL') THEN RAISE_APPLICATION_ERROR(-20024,
            'Le code etat civil ne doit pas etre null : ' || UnPatient.Etatcivil || ' - (C, M, D, V) attendu');
        END IF;
    WHEN OTHERS THEN RAISE;

END AjouterPatient;
SHOW ERRORS PROCEDURE AjouterPatient;

-- Test Ajouter patient

-- Pour affecter une valeur :=
DECLARE
    UnPatient Patients%ROWTYPE;
BEGIN
    -- Astuce : Select un patient et  modifier quelques valeurs
    UnPatient.NrSis := 'xxx';
    UnPatient.Nom := 'Dupondd';
    UnPatient.Prenom := 'Jean';
    UnPatient.Sexe := 'F';
    UnPatient.EtatCivil := 'M';
    UnPatient.Nationalite := 'BE';
    UnPatient.DateNaissance := TO_DATE('14/01/97', 'DD/MM/RR'); -- Toujours TO_DATE pour les dates
    
    AjouterPatient(UnPatient);
                    
    DBMS_OUTPUT.PUT_LINE('Ajout effectue');
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
-- Dans le bloc de test ne faire que le When others

-- Dans les tests inserer un nouveau patient avec tout correcte
-- Tester toute les exceptions de la procedure
-- Afficher le message de retour et afficher un message si tout se passe bien

SELECT * FROM PATIENTS WHERE Nom = 'Dupondd' AND Prenom = 'Jean';
SELECT * FROM MUTUELLES;
SELECT * FROM ADRESSES ORDER BY CODEADRESSE;
SELECT * FROM PAYS ORDER BY CODEPAYS;

SET SERVEROUTPUT ON;
