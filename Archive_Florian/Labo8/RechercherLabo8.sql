CREATE OR REPLACE PACKAGE GestionPatients IS
    FUNCTION Rechercher(UnLibelle IN Specialites.Libelle%TYPE DEFAULT 'MEDECINE GENERALE')
        RETURN Adresses.Localite%TYPE;
END GestionPatients;
/
-- IS = AS
-- Ajouter dans le meme package

CREATE OR REPLACE /*PACKAGE BODY GestionPatients AS  */
FUNCTION RechercherLabo8(UnLibelle IN Specialites.Libelle%TYPE DEFAULT 'MEDECINE GENERALE')
    RETURN Adresses.Localite%TYPE
AS
    UneLocalite Adresses.Localite%TYPE;
    LocLibelle Specialites.Libelle%TYPE;
BEGIN
    LocLibelle := COALESCE(UnLibelle, 'MEDECINE GENERALE');

    SELECT Localite /*INTO UneLocalite*/
    FROM Adresses
        INNER JOIN Medecins USING(CodeAdresse)
        INNER JOIN Specialites USING(Specialite)
    WHERE UPPER(Libelle) = 'MEDECINE GENERALE'
    GROUP BY Localite
    HAVING COUNT(*) = (SELECT MAX(COUNT(*))                       --               
                        FROM MEDECINS
                            INNER JOIN Specialites USING(Specialite)
                            INNER JOIN Adresses USING(CodeAdresse)
                        WHERE UPPER(Libelle) = 'MEDECINE GENERALE'
                        GROUP BY Localite);
                        
    RETURN UneLocalite;
EXCEPTION
    WHEN TOO_MANY_ROWS THEN
        RAISE_APPLICATION_ERROR(-20031, 'Trop de localites pour la specialite ' || UnLibelle);
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20032, 'Pas de localite pour la specialite ' || UnLibelle);
    WHEN OTHERS THEN RAISE;

END RechercherLabo8;
--END GestionPatients;
/
SELECT Localite
    FROM Adresses
        INNER JOIN Medecins USING(CodeAdresse)
        INNER JOIN Specialites USING(Specialite)
    WHERE UPPER(Libelle) = UPPER('DERMATOLOGIE')
    GROUP BY Localite
    HAVING COUNT(*) = (SELECT MAX(COUNT(*)) -- >= SELECT COUNT normalement idem
                        FROM MEDECINS
                            INNER JOIN Specialites USING(Specialite)
                            INNER JOIN Adresses USING(CodeAdresse)
                        WHERE UPPER(Libelle) = UPPER('DERMATOLOGIE')
                        GROUP BY Localite);
                      
DECLARE 
    loc VARCHAR2(50);
BEGIN 
    loc := Rechercher_Medecin_Specialite('OPHTALMOLOGIE');
    DBMS_OUTPUT.PUT_LINE('localit� : ' || loc);
END;


-- Fonction de Recherche de l'exam

SELECT med.NOM, med.PRENOM
    FROM MEDECINS med
        INNER JOIN PATIENTSMEDECINS patmed on med.NRMEDECIN = patmed.NRMEDECIN
        INNER JOIN PATIENTS pat on pat.NRSIS = patmed.NRSIS
    WHERE pat.ETATCIVIL = UPPER('c')
    GROUP BY med.NOM, med.PRENOM
    HAVING COUNT(*) = (SELECT MAX(COUNT(*))
                        FROM PATIENTS p
                            INNER JOIN PATIENTSMEDECINS pm ON p.NRSIS = pm.NRSIS
                            INNER JOIN MEDECINS med  ON med.NRMEDECIN = pm.NRMEDECIN
                        WHERE p.ETATCIVIL = UPPER('c')
                        GROUP BY med.NOM,med.PRENOM);