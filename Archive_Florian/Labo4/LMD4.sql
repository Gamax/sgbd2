-- Jointure
SELECT Nom As "Nom",
    Prenom AS "Prenom"
    
    FROM MEDECINS med
        LEFT OUTER JOIN HOPITAUXSERVICES hp ON hp.chefservice = med.NRMEDECIN
    
    WHERE ETATCIVIL = 'C'
        AND SEXE = 'M'
        AND CHEFSERVICE IS NULL
        
    ORDER BY 1, 2;

-- Ensembliste
SELECT Nom As "Nom",
    Prenom AS "Prenom"
    
    FROM MEDECINS med
        LEFT OUTER JOIN HOPITAUXSERVICES hp ON hp.chefservice = med.NRMEDECIN
    
    WHERE ETATCIVIL = 'C'
        AND SEXE = 'M'
        AND CHEFSERVICE IS NULL

MINUS
    
SELECT Nom As "Nom", Prenom AS "Prenom"
     FROM MEDECINS med
        INNER JOIN HOPITAUXSERVICES hp ON hp.chefservice = med.NRMEDECIN
    
    WHERE ETATCIVIL = 'C'
        AND SEXE = 'M'
        AND CHEFSERVICE IS NULL 
    ORDER BY 1, 2;
    
-- Imbriquee
SELECT Nom As "Nom", Prenom AS "Prenom"
    FROM MEDECINS med
    
    WHERE ETATCIVIL = 'C'
        AND SEXE = 'M'
        AND med.NRMEDECIN NOT IN 
            (SELECT CHEFSERVICE
                FROM hopitauxservices)
    ORDER BY 1, 2;
    
-- Correlee
SELECT Nom As "Nom", Prenom AS "Prenom"
    FROM MEDECINS med
    
    WHERE ETATCIVIL = 'C'
        AND SEXE = 'M'
        AND NOT EXISTS (SELECT CHEFSERVICE
                    FROM hopitauxservices hs
                    WHERE med.nrmedecin = hs.chefservice)
    ORDER BY 1, 2;