INSERT INTO ADRESSES VALUES(4011, 'Rue du centre', 'Yvoir', 5530);

INSERT INTO PATIENTS VALUES
('75071400679', 'Dupont', 'Lea', 'F', 'C', 'FR', TO_DATE('14/07/1975', 'DD/MM/RRRR'), NULL, NULL, NULL, NULL, 609, 4011);

INSERT INTO ALLERGIES VALUES('75071400679', 'S005');
INSERT INTO PATIENTSMEDECINS VALUES('75071400679', '10004080760');

COMMIT;

-- Obtenir le nrSIS avec le YYMMDD XXXXX le plus grand
SELECT NRSIS
    FROM PATIENTS
    ORDER BY SUBSTR(NRSIS, 7) DESC;
    
-- Obtenir le chef de service
SELECT *
    FROM MEDECINS med
        INNER JOIN HOPITAUXSERVICES hs ON med.NRMEDECIN = hs.chefservice
        INNER JOIN HOPITAUX h ON h.nrhopital = hs.nrhopital
        INNER JOIN SPECIALITES sp ON sp.specialite = med.specialite
    WHERE UPPER(h.nom) LIKE UPPER('Clinique Saint-Vincent')
        AND  UPPER(sp.Libelle) LIKE UPPER('Neuropsychiatrie');
        
-- Obtenir codeine
SELECT * FROM SUBSTANCES
    WHERE UPPER(NOM) LIKE UPPER('Codeine');
    
-- Verif
SELECT *
    FROM PATIENTS
    WHERE UPPER(Nom) LIKE UPPER('Dupont')
        AND UPPER(Prenom) LIKE UPPER('Lea');
        
SELECT *
    FROM PATIENTSMEDECINS
    WHERE NRSIS LIKE '75071400679';