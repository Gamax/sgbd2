-- Jointure NE FONCTIONNE PAS
SELECT pa.Nom AS "Nom",
    pa.Prenom AS "Prenom"
    -- TRUNC(months_between(CURRENT_DATE, pa.DATENAISSANCE) /12) AS "Age"
    
    FROM PATIENTS pa
        INNER JOIN PATIENTSMEDECINS pm ON pa.NRSIS = pm.NRSIS
        INNER JOIN MEDECINS med ON med.NRMEDECIN = pm.NRMEDECIN
    WHERE 
        EXTRACT(YEAR FROM pa.DateNaissance) BETWEEN 1970 AND 1979
        AND ((UPPER(med.NOM) LIKE UPPER('Munnix') AND UPPER(med.PRENOM) LIKE UPPER('Geoffroy'))
        AND (UPPER(med.NOM) LIKE UPPER('Kaison') AND UPPER(med.PRENOM) LIKE UPPER('Lala')))
        
        ORDER BY 1;
        
SELECT *
FROM PATIENTS pa
        INNER JOIN PATIENTSMEDECINS pm ON pa.NRSIS = pm.NRSIS
        INNER JOIN MEDECINS med ON med.NRMEDECIN = pm.NRMEDECIN
        WHERE pa.Nom = 'MASUIR';
        
Select *
FROM PATIENTS;

-- Esembliste
SELECT pa.Nom AS "Nom",
    pa.Prenom AS "Prenom"
    
    FROM PATIENTS pa
        INNER JOIN PATIENTSMEDECINS pm ON pa.NRSIS = pm.NRSIS
        INNER JOIN MEDECINS med ON med.NRMEDECIN = pm.NRMEDECIN
    WHERE 
        EXTRACT(YEAR FROM pa.DateNaissance) BETWEEN 1970 AND 1979
        AND (UPPER(med.NOM) LIKE UPPER('Munnix') AND UPPER(med.PRENOM) LIKE UPPER('Geoffroy'))
       
INTERSECT

SELECT pa.Nom AS "Nom",
    pa.Prenom AS "Prenom"
    
    FROM PATIENTS pa
        INNER JOIN PATIENTSMEDECINS pm ON pa.NRSIS = pm.NRSIS
        INNER JOIN MEDECINS med ON med.NRMEDECIN = pm.NRMEDECIN
    WHERE 
        EXTRACT(YEAR FROM pa.DateNaissance) BETWEEN 1970 AND 1979
        AND (UPPER(med.NOM) LIKE UPPER('Kaison') AND UPPER(med.PRENOM) LIKE UPPER('Lala'))
        
        ORDER BY 1, 2;

-- Imbriquee
SELECT pa.Nom AS "Nom",
    pa.Prenom AS "Prenom"
    
    FROM PATIENTS pa
        INNER JOIN PATIENTSMEDECINS pm ON pa.NRSIS = pm.NRSIS
        INNER JOIN MEDECINS med ON med.NRMEDECIN = pm.NRMEDECIN
    WHERE 
        EXTRACT(YEAR FROM pa.DateNaissance) BETWEEN 1970 AND 1979
        AND (UPPER(med.NOM) LIKE UPPER('Munnix') AND UPPER(med.PRENOM) LIKE UPPER('Geoffroy'))
        AND pa.NRSIS IN 
        (SELECT NRSIS
            FROM PATIENTSMEDECINS
                INNER JOIN MEDECINS USING(NRMEDECIN)
            WHERE 
                UPPER(NOM) LIKE UPPER('Kaison') AND UPPER(PRENOM) LIKE UPPER('Lala'))
    ORDER BY 1, 2;
    
-- Correlee
SELECT pa.Nom AS "Nom",
    pa.Prenom AS "Prenom"
    
    FROM PATIENTS pa
        INNER JOIN PATIENTSMEDECINS pm ON pa.NRSIS = pm.NRSIS
        INNER JOIN MEDECINS med ON med.NRMEDECIN = pm.NRMEDECIN
    WHERE 
        EXTRACT(YEAR FROM pa.DateNaissance) BETWEEN 1970 AND 1979
        AND (UPPER(med.NOM) LIKE UPPER('Munnix') AND UPPER(med.PRENOM) LIKE UPPER('Geoffroy'))
        AND EXISTS
        (SELECT pm2.NRSIS
            FROM PATIENTSMEDECINS pm2
                INNER JOIN MEDECINS USING(NRMEDECIN)
            WHERE 
                UPPER(NOM) LIKE UPPER('Kaison') AND UPPER(PRENOM) LIKE UPPER('Lala')
                AND pa.NRSIS = pm2.NRSIS)
    ORDER BY 1, 2;