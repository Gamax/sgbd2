-- Labo 7 sans la fonction rechercher 

set serveroutput on
-- Procedure 1
create or replace procedure AjoutPatient (NewPatient In Patients %Rowtype)
As
    InvalidReference Exception;
    ApplicConstr Exception;
    Pragma exception_init (InvalidReference , -2291);
    Pragma exception_init (ApplicConstr , -2290);
begin
    Insert into Patients values NewPatient;
    Commit;
exception
    when InvalidReference then 
        if(SQLERRM Like '%PAYS%') Then
            Raise_Application_Error (-20001, 'Le code pays ' || NewPatient.Nationalite || ' n''existe pas');
        Elsif(SQLERRM Like '%MUTUELLE%') Then
            Raise_Application_Error (-20002, 'Le code mutuelle ' || NewPatient.CodeMutuelle || ' n''existe pas');
        Elsif(SQLERRM Like '%ADRESSE%') Then
            Raise_Application_Error (-20003, 'L''adresse ' || NewPatient.CodeAdresse || ' n''existe pas');
        else
            DBMS_Output.Put_Line(SQLERRM);
            -- ...
        End if;
        
    When ApplicConstr then 
        if(SQLERRM Like '%CKPATIENTSEX%') then
            Raise_Application_Error(-20010, 'Le Sexe ' || NewPatient.Sexe || ' doit etre egal � M ou F');
        elsif (SQLERRM Like '%CKPATIENTETAT%') then
            Raise_Application_Error(-20011, 'L''etat Civil ' || NewPatient.EtatCivil || ' doit etre egal � C,M,D ou V');
        elsif (SQLERRM Like '%GRPSANG%') then
            Raise_Application_Error(-20012, 'Le groupe sanguin ' || NewPatient.GrpSanguin || ' doit etre egal � A,B,O ou AB');
        elsif (SQLERRM Like '%CPTBANCAIRE%') then
            Raise_Application_Error(-20013, 'Le compte bancaire est invalid ' || NewPatient.CptBancaire);
        elsif (SQLERRM Like '%NNPATIENTETATCI%') then
            Raise_Application_Error(-20014, 'L''etat civil ne doit pas etre null ' || NewPatient.EtatCivil);
        elsif (SQLERRM Like '%NNSEXEPAT%') then
            Raise_Application_Error(-20015, 'Le sexe ne doit pas etre null ' || NewPatient.Sexe);
        else
            DBMS_Output.Put_Line(SQLERRM);
        END IF;
    When others then raise;
End Ajoutpatient;
/            
            
-- bloc pour tester la procedure 

Declare 
    Nvpatient Patients %rowtype;
begin
    Nvpatient.NrSis := '910421';
    Nvpatient.Nom := 'Dussau';
    Nvpatient.Prenom := 'Florian'; 
    Nvpatient.Sexe := 'X'; 
    Nvpatient.EtatCivil := 'M'; 
    Nvpatient.Nationalite := 'FR'; 
    Nvpatient.DateNaissance := To_DATE('21/04/1991','DD/MM/YYYY'); 
    Nvpatient.CptBancaire := '123456789101';  
    Nvpatient.GrpSanguin := 'AB';    
    Nvpatient.Taille := 175;			
    Nvpatient.Poids := 75;		
    Nvpatient.CodeMutuelle := '101';
    Nvpatient.CodeAdresse := 4000;
    AjoutPatient(Nvpatient);
    
    DBMS_Output.Put_Line('Ajout OK');
Exception
    When others then DBMS_Output.PUT_Line(SQLERRM);
    
end;
/
-- Procedure 2

create or replace Procedure ModifierPatient (NewPatient In Patients%rowtype , AncienPatient In Patients%rowtype)
As
    InvalidReference Exception;
    ApplicConstr Exception;
    ExcBusy Exception;
    ExcDiff Exception;
    Pragma exception_init (InvalidReference , -2291);
    Pragma exception_init (ApplicConstr , -2290);
    Pragma exception_init (ExcBusy , -54);
    TupleCourant Patients%rowtype;
    Compteur NUMBER(1) := 0;
    Selection Number(1) := 1;

Begin
    While Selection = 1 LOOP
    Begin
        Select * into TupleCourant
        from Patients
        where NrSis = AncienPatient.NrSis For update nowait;
        
        Selection := 0;
        
    Exception 
        when ExcBusy Then 
            if Compteur < 3 then 
             DBMS_LOCK.SLEEP(2);
             Compteur := Compteur + 1;
            else
                raise;
            end if;
        end;
    end loop;
     if(
        COALESCE ( AncienPatient.Nom ,'NULL') <> COALESCE ( TupleCourant.Nom ,'NULL')
        OR COALESCE ( AncienPatient.Prenom ,'NULL') <> COALESCE ( TupleCourant.Prenom ,'NULL') 
        OR AncienPatient.Sexe <> TupleCourant.sexe
        OR AncienPatient.EtatCivil <> TupleCourant.EtatCivil
        OR COALESCE ( AncienPatient.Nationalite ,'NULL') <> COALESCE ( TupleCourant.Nationalite ,'NULL') 
        OR trunc(AncienPatient.Datenaissance) <> trunc(TupleCourant.Datenaissance)
        OR COALESCE ( AncienPatient.CptBancaire ,'NULL') <> COALESCE ( TupleCourant.CptBancaire ,'NULL') 
        OR COALESCE ( AncienPatient.GrpSanguin ,'NULL') <> COALESCE ( TupleCourant.GrpSanguin ,'NULL') 
        OR AncienPatient.Taille <> TupleCourant.Taille 
        OR AncienPatient.Poids  <> TupleCourant.Poids 
        OR COALESCE ( AncienPatient.CodeMutuelle ,'NULL') <> COALESCE ( TupleCourant.CodeMutuelle ,'NULL')
        OR AncienPatient.CodeAdresse <> TupleCourant.CodeAdresse)
     then 
        Raise ExcDiff;
    end if;
    UPDATE Patients
    Set row = NewPatient
    where NrSis = AncienPatient.NrSis;
    
    Commit;

Exception

    When ExcBusy Then 
        Raise_Application_Error(-20010, 'Le tuple ' || AncienPatient.NrSis || ' est occup�');
    WHen NO_DATA_FOUND Then
        Raise_Application_Error(-20011, 'Le tuple ' || AncienPatient.NrSis || ' n''existe pas');
    When ExcDiff Then
        Raise_Application_Error(-20012, 'Le tuple ' || AncienPatient.NrSis || ' est different du Patient qu''on veut modifier');
    
    when InvalidReference then 
        if(SQLERRM Like '%PAYS%') Then
            Raise_Application_Error (-20001, 'Le code pays ' || NewPatient.Nationalite || ' n''existe pas');
        Elsif(SQLERRM Like '%MUTUELLE%') Then
            Raise_Application_Error (-20002, 'Le code mutuelle ' || NewPatient.CodeMutuelle || ' n''existe pas');
        Elsif(SQLERRM Like '%ADRESSE%') Then
            Raise_Application_Error (-20003, 'L''adresse ' || NewPatient.CodeAdresse || ' n''existe pas');
        else
            DBMS_Output.Put_Line(SQLERRM);
        End if;
        
    When ApplicConstr then 
        if(SQLERRM Like '%CKPATIENTSEX%') then
            Raise_Application_Error(-20020, 'Le Sexe ' || NewPatient.Sexe || ' doit etre egal � M ou F');
        elsif (SQLERRM Like '%CKPATIENTETAT%') then
            Raise_Application_Error(-20021, 'L''etat Civil ' || NewPatient.EtatCivil || ' doit etre egal � C,M,D ou V');
        elsif (SQLERRM Like '%GRPSANG%') then
            Raise_Application_Error(-20022, 'Le groupe sanguin ' || NewPatient.GrpSanguin || ' doit etre egal � A,B,O ou AB');
        elsif (SQLERRM Like '%CPTBANCAIRE%') then
            Raise_Application_Error(-20023, 'Le compte bancaire est invalid ' || NewPatient.CptBancaire);
        elsif (SQLERRM Like '%NNPATIENTETATCI%') then
            Raise_Application_Error(-20024, 'L''etat civil ne doit pas etre null ' || NewPatient.EtatCivil);
        elsif (SQLERRM Like '%NNSEXEPAT%') then
            Raise_Application_Error(-20025, 'Le sexe ne doit pas etre null ' || NewPatient.Sexe);
        else
            DBMS_Output.Put_Line(SQLERRM);
        END IF;
    When Others then 
    DBMS_Output.Put_Line('AAAHAHHHHHHHHHH !!!!');
    Raise;
End;
/

-- JEux de tests

Declare 
    Nvpatient Patients %rowtype;
    AncienPatient Patients %rowtype;
begin
    select * into Ancienpatient 
    from Patients
    where NrSis = 74070100291;
    
    Nvpatient.NrSis := '74070100291';
    Nvpatient.Nom := 'Dussau';
    Nvpatient.Prenom := 'Florian'; 
    Nvpatient.Sexe := 'M'; 
    Nvpatient.EtatCivil := 'M'; 
    Nvpatient.Nationalite := 'FR'; 
    Nvpatient.DateNaissance := To_DATE('21/04/1991','DD/MM/YYYY'); 
    Nvpatient.CptBancaire := '123456789101';  
    Nvpatient.GrpSanguin := 'AB';    
    Nvpatient.Taille := 175;			
    Nvpatient.Poids := 75;		
    Nvpatient.CodeMutuelle := '101';
    Nvpatient.CodeAdresse := 100;
    ModifierPatient(Nvpatient,AncienPatient);
    
    DBMS_Output.Put_Line('Ajout OK');
Exception
    When others then DBMS_Output.PUT_Line(SQLERRM);
    
end;
/

-- Trigger 4
CREATE OR REPLACE TRIGGER PatientsDelNrSis
BEFORE DELETE ON Patients -- Toujours pr?ciser les champs qui activent le declencheur
FOR EACH ROW
 -- Delete :OLD uniquement
BEGIN
    DELETE FROM PATIENTSMEDECINS
    WHERE nrsis = :OLD.nrsis;

    DELETE FROM ALLERGIES
    WHERE nrsis = :OLD.nrsis;

    EXCEPTION
        WHEN OTHERS THEN RAISE;
END;
/
SHOW ERRORS TRIGGER PatientsDelNrSIS;

--Procedure  3 

create or replace Procedure SupprimerPatient (Sis In Patients.NrSis %Type)
As
    InvalidReference Exception;
    ExcParamNull Exception;
    ExcMedeGen Exception;
    ExcCleEtrangere Exception;
    Pragma exception_init (ExcCleEtrangere , -2292);
    adressePatient Patients.CodeAdresse %TYPE;
BEGIN
    IF Sis IS NULL THEN 
        RAISE ExcParamNull;
    END IF;
    
    SELECT CodeAdresse INTO adressePatient
    FROM Patients
    WHERE NrSis = Sis;

    DELETE FROM Patients
    Where NrSis = Sis 
        AND NrSis NOT IN (  SELECT pm.NrSis
                            FROM Patientsmedecins pm, Medecins m, Specialites s
                            WHERE pm.NrMedecin = m.NrMedecin
                                AND m.Specialite = s.Specialite
                                AND s.Libelle = 'MEDECINE GENERALE');
    IF SQL%NOTFOUND THEN 
        RAISE ExcMedeGen;
    END IF;
    
    BEGIN 
        DELETE FROM adresses
        WHERE CodeAdresse = adressePatient;
    EXCEPTION 
        WHEN ExcCleEtrangere THEN 
            Raise_Application_Error (-20001, 'le code adresse ' || adressePatient || ' est utilis� dans d''autre tuples');
        WHEN OTHERS THEN
            DBMS_Output.PUT_Line(SQLERRM || ' toto');
            RAISE;
    END;

Exception
    WHen NO_DATA_FOUND Then
        Raise_Application_Error(-20011, 'Le tuple ' || Sis || ' n''existe pas');
    WHen ExcMedeGen Then
        Raise_Application_Error(-20012, 'Le tuple ' || Sis || ' a un medecin generaliste');
    WHen ExcParamNull Then
        Raise_Application_Error(-20013, 'Le parametre de la procedure est null !');
    When OTHERS Then
        DBMS_Output.PUT_Line(SQLERRM || ' toto');
        
END;
/

-- Jeux de tests 

BEGIN
    SupprimerPatient('910421');
END;
/


-- Fonction de Recherche 

-- Fonction Rechercher Labo 7
set serveroutput on
CREATE OR REPLACE FUNCTION Rechercher
    RETURN Medecins%ROWTYPE 
AS
    UnMedecin Medecins%ROWTYPE;
BEGIN
    SELECT M.*  INTO UnMedecin
    FROM MEDECINS M 
        INNER JOIN PATIENTSMEDECINS PM ON PM.NrMedecin = M.NrMedecin
        INNER JOIN PATIENTS P ON PM.NrSis = P.NrSis
        INNER JOIN ADRESSES A ON A.CodeAdresse = P.CodeAdresse
    WHERE UPPER(A.LOCALITE) = UPPER('Saint-Nicolas')
INTERSECT
    SELECT M.*
    FROM MEDECINS M 
        INNER JOIN PATIENTSMEDECINS PM ON PM.NrMedecin = M.NrMedecin
        INNER JOIN PATIENTS P ON PM.NrSis = P.NrSis
        INNER JOIN ADRESSES A ON A.CodeAdresse = P.CodeAdresse
    WHERE UPPER(A.LOCALITE) = UPPER('Liege');
    
    RETURN UnMedecin;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20200, 'Aucun medecin trouve.');
    WHEN TOO_MANY_ROWS THEN
        RAISE_APPLICATION_ERROR(-20201, 'Plusieurs medecins trouves.');
    WHEN OTHERS THEN RAISE;
END Rechercher;
/

-- Jeux de tests 

DECLARE
    UnMedecin Medecins%ROWTYPE;
BEGIN
    UnMedecin := Rechercher();
    DBMS_OUTPUT.PUT_LINE('Medecin : ' || UnMedecin.Prenom);
EXCEPTION    
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
/
insert into PatientsMedecins values (83031500433 , 10026580001); -- insertion dans PatientsMedecin d'un patient dont la localit� est Liege pour le medecin 1002658000

insert into PatientsMedecins values (84091600176 , 10026580001); -- insertion dans PatientsMedecin d'un patient dont la localit� est Saint-Nicolas pour le medecin 1002658000

DELETE FROM PatientsMedecins
    Where NrMedecin = '10026580001'; 
    
DELETE FROM PatientsMedecins
    Where NrMedecin = '10054782001';